README
======

Lookbook provides functionality that allows the users to create a 
image canvas called lookbook and plot and popup collection node
on canvas. The main feature of lookbook is to provide display of 
range of product / parts / features collection on single images 
called canvas image.

Lookbook provides users the ability to:
1. Create a widget field for content type to add canvas image.
2. Select node as collection on widget settings.
3. Plot collection node on canvas images using draggable plotter.
4. Lookbook page to display lookbook.

Dependencies:
1. Jquery Update
