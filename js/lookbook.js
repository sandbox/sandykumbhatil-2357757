(function ($) {
  Drupal.behaviors.lookbook = {
    attach: function(context, settings) {
      $( "#draggable" ).draggable({
        containment: "#containment-wrapper", 
        scroll: false,
      });
      $( "#containment-wrapper" ).droppable({
        cursor: "move",
        drop: function( event, ui ) {
          var adminleft = ui.position.left;
          var admintop = ui.position.top;
          $('#x').val(admintop);
          $('#y').val(adminleft);
          $('#pin').css({ top: admintop+'px' });
          $('#pin').css({left: adminleft+'px'});
        }
      });
      function position() {
        if( $("#info").length > 0)
        {
          $('#info').position({
            of: $('#pin'),
            my: 'right bottom',
            at: 'left top',
            collision: 'flipfit flipfit',
            within: $('#containment-wrapper-plotter'),
          });
        } 
      }
      $('#pin').on('mouseover',function(){
        $('#info').show();
        position();
      });
      $('#pin').mouseout(function(){
        $('#info').hide();
      });
      position();
    }
  };
})(jQuery);